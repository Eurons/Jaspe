#encoding: utf-8
require "zlib"
require "../core/PluginObject.rb"
a = PluginObject.new
a.set_info("NoMapLinker","Nuri_Yuri2","PSDK2")
a.add_mod(
  "Yuki::MapLinker",
  "    OffsetX = 10
    OffsetY = 7
    DeltaMaker = 3",
  :replace, 
  "    OffsetX = 0
    OffsetY = 0
    DeltaMaker = 0"
)
a.add_mod(
  "Yuki::MapLinker",
  "def test_warp",
  :add_after, 
  "return"
)

begin
	a.build()
rescue Exception
  puts $!.class
	puts $!.message
	system("pause")
end