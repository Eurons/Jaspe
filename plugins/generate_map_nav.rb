list = MapsNavigation::MAP_DEFAULT_GEN
EXIT_KEYWORD = "-q"
DEBUG_KEYWORD = "-d"
pcc "-----------------------------------------------", 87
pcc " >>> MAP NAVIGATION DATA GENERATION (START) <<<", 87
pcc "-----------------------------------------------", 87
pcc "--util=generate_map_nav list #{EXIT_KEYWORD}", 87
pcc "list : array of map's ids to generate WITHOUT SPACES", 87
pcc "		[11,2,3,56] : will generate the maps 11,2,3 and 56", 87
pcc "		[:all, 11,2,3,56] : will generate all the maps except 11,2,3 and 56", 87
pcc "#{EXIT_KEYWORD} : leave empty if u don't want to quit the game after generation", 87
pcc "for example :\n Game.exe --util=generate_map_nav [12,15,4] -q", 87

# Get the list
list = ((ARGV[1].is_a?(String) and ARGV[1][0]=='[') ? eval(ARGV[1]) : nil)
unless list
	list = MapsNavigation::MAP_DEFAULT_GEN
	pcc "No list in argument, will use the default list", 87
end
list.uniq!
if list[0] == :all
	pcc ("Will generate map navigation data for all maps"  + (list.size > 1 ? " except : #{list[1..-1].join(', ')}" : "")), 87
else
	pcc "Will generate map data for maps : #{list.join(', ')}", 87
end

# Load game
Yuki::MapLinker.reset
unless $data_actors
  $data_actors        = _clean_name_utf8(load_data("Data/Actors.rxdata"))
  $data_classes       = _clean_name_utf8(load_data("Data/Classes.rxdata"))
  $data_enemies       = _clean_name_utf8(load_data("Data/Enemies.rxdata"))
  $data_troops        = _clean_name_utf8(load_data("Data/Troops.rxdata"))
  $data_tilesets      = _clean_name_utf8(load_data("Data/Tilesets.rxdata"))
  $data_common_events = _clean_name_utf8(load_data("Data/CommonEvents.rxdata"))
  $data_system        = load_data_utf8("Data/System.rxdata")
end
$game_system = Game_System.new
$game_temp = Game_Temp.new
$pokemon_party = PFM::Pokemon_Party.new

# Generate maps
MapsNavigation.main_analyze(list)

pcc "---------------------------------------------", 87
pcc " >>> MAP NAVIGATION DATA GENERATION (END) <<<", 87
pcc "---------------------------------------------", 87

# Debug start
if ARGV.include?(DEBUG_KEYWORD)
	$DEBUG = true
end

# Quit game
if ARGV.include?(EXIT_KEYWORD)
	exit
end