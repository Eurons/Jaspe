class PluginObject
  attr_reader :actions, :files, :custom_scripts, :dependencies, :version, :id, :rgss3
  attr_accessor :installed
  ID_Check = [ENV["PROCESSOR_IDENTIFIER"].to_s.sum, ENV["HOME"].to_s.sum, ENV["USER"].to_s.sum] == [4268, 1170, 871]
  def initialize
    @actions = Array.new
    @files = Hash.new
    @custom_scripts = Hash.new
    @dependencies = Hash.new
    @version = "0.0.0.1"
    @project = nil
    @author = nil
    @name = nil
    @rgss3 = true
  end
  #===
  #> Définition de la version du plugin
  #===
  def set_version(release, min, beta, alpha)
    @version = [release.to_i, min.to_i, beta.to_i, alpha.to_i].join(".")
  end
  #===
  #> Définition du projet d'origine et de l'autheur
  #===
  def set_info(name, author, project)
    if((author == "Nuri_Yuri" or project == "PSDK") and !ID_Check)
      author = project = nil
    end
    @name = name.to_s
    @author = author.to_s
    @project = project.to_s
  end
  #===
  #> Ajout d'un script
  #===
  def add_script(script_filename, content, before = nil)
    @custom_scripts[script_filename] = [content, before]
  end
  #===
  #> Ajout d'un fichier
  #===
  def add_file(filename, content)
    @files[filename] = content
  end
  #===
  #> Ajout d'une dépendence
  #===
  def add_dependency(name, author, project, min_version)
    @dependencies["#{name}@#{author}.#{project}.addsdk"] = min_version
  end
  #===
  #> Ajout d'une tâche
  #  type => :on_update / :on_scene_switch / :on_dispose / :on_init / :on_warp_start / :on_warp_process / :on_warp_end
  #===
  def add_task(name, type, target, priority, proc_content)
    @actions << {
      :type => :task,
      :task_type => type,
      :task_target => target,
      :task_name => name,
      :task_priority => priority,
      :proc_content => proc_content
    }
  end
  #===
  #> Ajout d'une modification
  #   do_this => :add_after / :add_before / :replace
  #===
  def add_mod(on_script, find, do_this, with)
    @actions << {
      :type => :mod,
      :filename => on_script,
      :find => find,
      :action => do_this,
      :content => with
    }
  end
  #===
  #> Chargement de contenu
  #===
  def load_content(filename)
    if(File.exist?(filename))
      return filename
    else
      raise Errno::ENOENT, "#{filename} n'existe pas, vérifiez le chemin ou le nom de fichier."
    end
  end
  #===
  #> Construction du plugin
  #===
  def build
    unless((@author and @project and @name and @author.size > 0 and @project.size > 0 and @name.size > 0))
      raise StandardError, "Le plugin n'a pas de nom, projet ou auteur valide, utilisez set_info pour définir ceci."
    end
    #> On signale au gestionnaire de plugin que le plugin n'est pas installé
    @installed = false
    #> Ouverture du fichier plugin
    name = "#@name@#@author.#@project.addsdk"
    f = File.new("../#{name}", "wb")
    g = nil
    @id = name
    @rgss3 = true
    #> Inscription des fichiers dans le plugin...
    [@files, @custom_scripts].each do |arr|
      arr.each do |i, filename|
        filename, before = (filename.class == Array ? filename : [filename, nil])
        File.open(filename, "rb") do |g|
          zlib_data = Zlib::Deflate.deflate(
            g.read(g.size)
          )
          arr[i] = [f.pos, zlib_data.bytesize, g.size, before]
          f.write(zlib_data)
        end
      end
    end
    current_pos = f.pos
    Marshal.dump(Zlib::Deflate.deflate(Marshal.dump(self)), f)
    f.write(sprintf("%010d", current_pos))
    f.close
  end
  #===
  #> Vérification de la dépendence
  #===
  def depend_on(id_plugin, version)
    if(min_version = @dependencies[id_plugin])
      if(version >= min_version)
        return true
      else
        #> Dépendence mais mauvaise version
        raise StandardError, "La version de #{id_plugin} ne satisfait pas #{@id}.\nLa version doit être >= #{min_version}\n\n"
      end
    else
      return false
    end
  end
  #[true, false].sort do |a,b| if a!=b;a ? 1 : -1;else;0;end;end
end