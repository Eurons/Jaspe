#encoding: utf-8
require "json"
require "net/http"
require "base64"
require "zlib"

require_relative "ScriptAnalyzer"
# Module that manage the plugins and help them to be installed
module Plugin
  class Error < StandardError
  end
  module_function
  # Install all the plugin that needs to be installed
  def install_all
    Dir.mkdir!("plugins/cache")
    load_all
    load_scripts
    apply_all
    validate_everything
  end
  # Load all the plugins that needs to be installed
  def load_all
    @to_install = []
    Dir["plugins/*.json"].each { |plugin_name| @to_install << load(plugin_name) }
  end
  # Load a plugin and return a plugin object type
  # @param obj [String, File] can be, a file, a filename or a JSON contents
  # @return [Hash] the hash has reader methods that access to the keys
  def load(obj)
    if obj.is_a?(File)
      obj = obj.read(obj.size)
    else
      if obj[0] != "{"
        File.open(obj, "r:BOM|utf-8") do |f| obj = f.read(f.size) end
      end
    end
    obj.force_encoding(Encoding::UTF_8)
    hash = JSON.parse(obj, symbolize_names: true)
    return methodize(hash)
  end
  # Create methods that helps to acces to the data of the Hash
  # @param hash [Hash]
  # @return [Hash] hash
  def methodize(hash)
    def hash.name
      self[:name] || "Iconnu"
    end
    def hash.author
      self[:author] || "Anonyme"
    end
    def hash.version
      self[:version] || "0.0.0.1"
    end
    def hash.psdk_version_min
      self[:psdk_version_min] || 5896
    end
    def hash.url
      self[:url] || nil
    end
    def hash.banner
      self[:banner] || nil
    end
    def hash.description
      self[:description] || "Plugin sans description."
    end
    def hash.tags
      self[:tags] || ["Uncategorized"]
    end
    def hash.credits
      self[:credits] || []
    end
    def hash.mods
      self[:mods] || []
    end
    def hash.methods(param = false)
      return super() if param
      self[:methods] || []
    end
    def hash.constants
      self[:constants] || []
    end
    def hash.scripts
      self[:scripts] || []
    end
    def hash.ressources
      self[:ressources] || []
    end
    return hash
  end
  # Load all the script from PSDK
  def load_scripts
    @accepted_mods = [] # Store all the mods that could be done when checked the first time (applied at the end)
    @scripts_info = {} # Tell us the add after, replace and other stuff related a specific script
    @methods_to_add = {} # Store all the methods that should be added in each classes (added at the very end)
    @script_analyze = ScriptAnalyzer.new
    psdk_scripts = load_data("#{PSDK_PATH}/Data/Scripts.rxdata")
    cc 0x03
    puts "Anyalyzing scripts..."
    cc 0x07
    psdk_scripts.each_with_index do |script, i|
      print("\r[#{("=" * ( (i+1) * 40 / psdk_scripts.size)).ljust(40)}]")
      @script_analyze.analyze(script[1].force_encoding(Encoding::UTF_8), Zlib::Inflate.inflate(script[2]).force_encoding(Encoding::UTF_8))
      GC.start
    end
    cc 0x02
    puts "\nDone..."
  end
  # Apply all the found plugins
  def apply_all
    cc 0x03
    puts "Applying plugins"
    cc 0x07
    @to_install.each do |plugin|
      apply(plugin)
    end
    #< Dont forget to validate
  end
  # Apply a specific plugin
  # @param plugin [Hash]
  def apply(plugin)
    puts "Applying \e[34m#{plugin.name}\e[37m by \e[35m#{plugin.author}\e[37m"
    plugin.ressources.each do |ressource|
      apply_ressource(ressource)
    end
    plugin.scripts.each do |script|
      apply_script(script)
    end
    plugin.mods.each do |mod|
      apply_mod(mod)
    end
    plugin.methods.each do |meth|
      apply_method(meth)
    end
    plugin.constants.each do |constant|
      apply_constant(constant)
    end
  end
  # Apply ressource from a plugin
  # @param ressource [Hash]
  def apply_ressource(ressource)
    puts ressource
  end
  # Apply the script from a plugin
  # @param script [Hash]
  def apply_script(script)
    raise Error, "Bad script definition. Don't know where to find the script..." unless script[:url]
    download_script(script[:url])
    affected_script = (script[:replace] || script[:place_after] || script[:place_before])
    raise Error, "Bad script definition. Don't know what to do with the script..." unless affected_script
    unless @scripts_info[affected_script]
      @scripts_info[affected_script] = {replace: [], place_after: [], place_before: []}
    end
    if script[:replace]
      @scripts_info[affected_script][:replace] << script
    elsif script[:place_after]
      @scripts_info[affected_script][:place_after] << script
    elsif script[:place_before]
      @scripts_info[affected_script][:place_before] << script
    end
  end
  # Download a script
  # @param url [String] url of the script
  def download_script(url)
    return unless url.index('https://') == 0
    url_split = url.split('/')
    path = url_split[3..-2].join('/')
    Dir.chdir('plugins/cache') do
      Dir.mkdir!(path)
      filename = url_split[3..-1].join('/')
      unless File.exist?(filename)
        unless download_file(url, filename)
          File.delete(filename) if File.exist?(filename)
          raise Error, "Failed to download #{url}..."
        end
      end
    end
  end
  # Apply the modification to the scripts by a plugin
  # @param mod [Hash]
  def apply_mod(mod)
    correct_def = (mod[:replace] || mod[:place_after] || mod[:place_before])
    raise Error, "Bad modification definition." unless (_in = mod[:in]) and (search = mod[:search]) and correct_def
    data = @script_analyze.scripts[_in]
    raise Error, "Bad modification definition. Failed to find #{_in}." unless data
    data.each_with_index do |line, i|
      if line.include?(search)
        mod[:line] = i
        break
      end
    end
    raise Error, "Bad modification definition. Failed to find \e[36m#{search}\e[37m in #{_in}" unless mod[:line]
    mod[:script_data] = data
    @accepted_mods << mod
  end
  # Apply the methods from a plugin
  # @param meth [Hash]
  def apply_method(meth)
    raise Error, "Bad method definition." unless (_meth = meth[:method].split('(').first.to_sym) and (_class = meth[:class]) and (contents = meth[:contents])
    current_class = find_class(_class, "method")
    unless method_info = current_class[:methods][_meth]
      raise Error, "Bad method definition. Failed to find \e[36m#{_meth}\e[37m in #{_class}." unless meth[:type]
      puts "Failed to find \e[36m#{_meth}\e[37m in #{_class}. Method will be added at the end of the process."
      @methods_to_add[_class] = [] unless @methods_to_add[_class]
      return @methods_to_add[_class] << meth
    end
    apply_method_to_script(contents, method_info, meth[:method])
  end
  # Find the class for a method or a constant
  # @param _class [String] the class definition
  # @param type [String] method or constant definition
  # @return [Hash] the class
  def find_class(_class, type)
    class_path = _class.split('::')
    class_path.shift if class_path.first.empty?
    class_path.pop if class_path.last.empty?
    class_path.collect! { |class_name| class_name.to_sym }
    current_class = @script_analyze.classes[:Object]
    class_path.each do |wanted_class|
      current_class = current_class[wanted_class]
      raise Error, "Bad #{type} definition. Failed to find \e[36m#{wanted_class}\e[37m in #{_class}." unless current_class
    end
    return current_class
  end
  # Apply the method modification to a script
  # @param contents [String] the contents of the modification
  # @param method_info [Hash] info about the original method
  # @param definition [String] the definition of the method in the plugin
  def apply_method_to_script(contents, method_info, definition)
    if method_info[:begin] == method_info[:end]
      raise Error, "#{definition} is defined in only one line. It's not compatible with the plugins method edition..."
    end
    script = @script_analyze.scripts[method_info[:script]]
    method_info[:begin].upto(method_info[:end] - 2) do |i|
      script[i].clear
    end
    if definition.include?('(')
      script[method_info[:begin] - 1] = definition
    end
    script[method_info[:begin]] = contents
  end
  # Apply the constant from a plugin
  # @param constant [Hash]
  def apply_constant(constant)
    raise Error, "Bad constant definition." unless (const = constant[:constant].to_sym) and (_class = constant[:class]) and (constant.has_key?(:value))
    current_class = find_class(_class, "constant")
    unless const_info = current_class[:constants][const]
      raise Error, "Bad method definition. Failed to find \e[36m#{const}\e[37m in #{_class}."
    end
    script = @script_analyze.scripts[const_info[:script]]
    script[const_info[:define] - 1] = parse_constant(constant)
  end
  # Parse a constant to add it to a script
  # @param constant [Hash]
  # @return [String]
  def parse_constant(constant)
    value = constant[:value]
    if value.is_a?(String) and value.index('ruby:') == 0
      value = value[5..-1]
    else
      value = value.inspect
    end
    if constant[:name]
      return "\# @note Added by #{constant[:name]}\n#{constant[:constant]} = #{value}"
    end
    return "#{constant[:constant]} = #{value}"
  end
  # Download a file and save it
  # @param src [String] SRC URL of the file to download
  # @param dest [String] the destination filename
  # @return [Boolean] if the operation was a success
  def download_file(src, dest)
    uri = URI(src)
    print "Requesting #{dest}\r"
    Net::HTTP.start(uri.host, uri.port, use_ssl: true, ca_file: './lib/cert.pem') do |http|
      request = Net::HTTP::Get.new uri
      http.request request do |response|
        return false if response.code.to_i != 200
        read = 0
        length = response.content_length
        File.open(dest, 'wb') do |f|
          puts "Donwloading #{dest}"
          t = Time.new
          response.read_body do |chunk|
            read += chunk.bytesize
            print "\r[", ("=" * (read * 10 / length)).ljust(10), '] '
            print (read * 100 / length).to_s.rjust(3), '% '
            print length / 1024, 'Ko '
            print (chunk.bytesize / (Time.new - t) / 1024).to_s.to_i, 'Ko/s        '
            t = Time.new
            f.write chunk
          end
        end
        print "\n"
        return true
      end
    end
    return false
  end
  # Function that validate the plugin work
  def validate_everything
    @accepted_mods.each do |mod|
      if (data = mod[:script_data])[line = mod[:line]].include?(mod[:search])
        if mod[:replace]
          data[line].sub!(mod[:search], mod[:replace])
        elsif mod[:place_after]
          if line >= data.size
            data << mod[:place_after]
          else
            data[line + 1] << "\n#{mod[:place_after]}"
          end
        elsif mod[:place_before]
          if line >= 1
            data[line - 1] << "\n#{mod[:place_before]}"
          else
            data[0] = "#{mod[:place_before]}\n#{data.first}"
          end
        end
      else
        raise Error, "Failed to apply modification on #{mod[:in]} search = #{mod[:search]}"
      end
    end
    add_method_script = generate_add_method_script
    scripts = generate_scripts
    scripts << add_method_script
    File.open("Data/Script_Plugin.rxdata", "wb") { |f| Marshal.dump(scripts, f) }
  end
  # Functhin that generates the scripts
  # @return [Array]
  def generate_scripts
    script_info = @scripts_info
    order = @script_analyze.order
    scripts = @script_analyze.scripts
    script_array = []
    order.each do |script_name|
      if sc_info = script_info[script_name]
        sort_scripts(sc_info)
        sc_info[:place_before].each { |script| script_array << [0, script[:name], fetch_script_contents(script[:url])] }
        if sc_info[:replace].size > 0
          script_array << [0, sc_info[:replace][0][:name], fetch_script_contents(sc_info[:replace][0][:url])]
        else
          script_array << [0, script_name, Zlib::Deflate.deflate(scripts[script_name].join("\n"))]
        end
        sc_info[:place_after].each { |script| script_array << [0, script[:name], fetch_script_contents(script[:url])] }
        next
      end
      script_array << [0, script_name, Zlib::Deflate.deflate(scripts[script_name].join("\n"))]
    end
    return script_array
  end
  # Function that fetch the script contents of a plugin script and compress it if not already compressed
  # @param url [String] info about how to find the script
  # @return [String]
  def fetch_script_contents(url)
    if url.index('https://') == 0
      url_split = url.split('/')
      filename = url_split[3..-1].join('/')
      Dir.chdir('plugins/cache') do
        File.open(filename, "r:BOM|utf-8") do |f|
          return Zlib::Deflate.deflate(f.read(f.size))
        end
      end
    elsif url.index('file://./') == 0
      File.open(url[9..-1], "r:BOM|utf-8") do |f|
        return Zlib::Deflate.deflate(f.read(f.size))
      end
    elsif url.index('base64://') == 0
      return Base64.decode64(url[9..-1])
    end
    return Zlib::Deflate.deflate("")
  end
  # Sort the scripts by priority
  # @note Lower number is the first in the array
  # @param sc_info [Hash]
  def sort_scripts(sc_info)
    sc_info[:place_before].sort! { |a, b| a[:priority] <=> b[:priority] }
    sc_info[:replace].sort! { |a, b| a[:priority] <=> b[:priority] }
    sc_info[:place_after].sort! { |a, b| a[:priority] <=> b[:priority] }
  end
  # Function that generate the add_method script
  # @return [Array]
  def generate_add_method_script
    script = ""
    @methods_to_add.each do |_class, methods|
      if methods.first[:type] == 'class'
        script << "class #{_class}\n"
      else
        script << "module #{_class}\n"
      end
      methods.each do |method_info|
        script << "def #{method_info[:method]}\n#{method_info[:contents]}\n"
        if method_info[:type] == 'module_function'
          script << "module_function :#{method_info[:method].split('(').first}\n"
        end
        script << "end\n"
      end
      script << "end\n"
    end
    return [0, "Methods added by Plugin", Zlib::Deflate.deflate(script)]
  end
end